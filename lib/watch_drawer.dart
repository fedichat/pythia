import 'package:flutter/material.dart';

class Watcher {
  List<_RoomData> rooms = [ _RoomData("1,1","The Hub") ];

  // This lets us switch rooms in the parent
  Function switchRoom;

  List<Widget> makeWidgets() {
    List<Widget> widgets = [
      Container(
        height: 120,
        child: DrawerHeader(
          //child: Container(
          //    height: 24.0,
          //    child: Icon(Icons.map),//Text('Saved Rooms'),
          //),
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
        )
      )];
    for (var room in rooms) {
      widgets.add(room.toWidget(switchRoom));
    }
    return widgets;
  }

  void addRoom(String id, String name) {
    rooms.add(_RoomData(id,name));    
  }
  void removeRoom(String name) {
    rooms.removeWhere((room) => room.shortcut == name);    
  }

  List<String> getIds() => rooms.map((data) => data.id).toList();

  String getName(String id) => rooms.where((room) => room.id == id)
                                    .map((room) => room.shortcut)
                                    .first;
  void new_messages(String roomId) {
    for (var data in rooms) {
      if (data.id == roomId) {
        data.new_messages = true;
      }
    }
  }

  void mark(String roomId) {
    for (var data in rooms) {
      if (data.id == roomId) {
        // TODO: How do I do phone notifications
        data.notified = true;
      }
    }
  }

  void clearFlags(String roomId) {
    for (var data in rooms) {
      if (data.id == roomId) {
        data.new_messages = false;
        data.notified = false;
      }
    }
  }

  Watcher(this.switchRoom);
}

// TODO: make switchRoom clear flags
// for when a user /gos into a room

class _RoomData {
  String id;
  String shortcut;
  bool notified = false;
  bool new_messages = false;

  _RoomData(this.id, this.shortcut);

  // TODO: Change text color when there's a notification
  Widget toWidget(Function switchRoom) {
    // Hightlight notifications
    // pings are bold and red
    // new messages = bold room name
    // otherwise plain black text
    TextStyle s;
    if (notified) {
      s = TextStyle(fontWeight: FontWeight.bold, color: Colors.red);
    } else if (new_messages) {
      s = TextStyle(fontWeight: FontWeight.bold);
    }

    return ListTile(
      title: Text(
          '$shortcut',
          style: s,
      ),
      onTap: () {
          notified = false;
          new_messages = false;
          switchRoom(id);
      },
    );
  }
}
