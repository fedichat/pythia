import 'package:http/http.dart' as http;

import 'dart:collection';
import 'dart:convert';
import 'dart:math';

import 'message.dart';

// s t r u c t
class MessageCache {
  List<Message> messages = [];
  int currentMessageId = -1;
}

class Cache {
  String _roomId;
  String _homeserver;
  HashMap<String,MessageCache> _messages = HashMap();

  void setRoom(String r) {
    _roomId = r;
    if (!_messages.containsKey(_roomId)) {
      _messages[_roomId] = MessageCache();
    }
  }
  void setServer(String s) {
    _homeserver = s;

    // Invalidate the whole cache
    _messages.clear();
  }

  void add(Message m) {
    _messages[_roomId].messages.add(m);
  }

  void addAll(List<Message> m, String roomId) {
     _messages[roomId].messages.addAll(m);
  }

  int getCurrentMessageID(String roomId) {
    return _messages[roomId].currentMessageId;
  }

  void setCurrentMessageID(int id, String roomId) {
    _messages[roomId].currentMessageId = id;
  }

  List<Message> getMessages() {
    return _messages[_roomId].messages ?? [];
  }
  Future<List<Message>> fetchRoomMessages(String roomId) async {
      var response;
      // Init room if it hasn't been already
      if (_messages[roomId] == null) {
          _messages[roomId] = MessageCache();
      }
      if (getCurrentMessageID(roomId) == -1) {
        response = await http.get('https://$_homeserver/_fedichat/v1/json/room/$roomId/messages');
      } else {
        response = await http.get('https://$_homeserver/_fedichat/v1/json/room/$roomId/messages?after=${getCurrentMessageID(roomId)}');
      }
      if (response.statusCode == 200) {
        // If the call to the server was successful, parse the JSON into a list of messages
        var message_list = json.decode(response.body) as List;
        var incoming_messages = message_list.reversed.map((m) => Message.fromJson(m)).toList();

        // Calculate the current message ID so we don't fetch any of the same
        // messages again
        var highest_id;

        try { 
          // Why does this cause 10 exceptions per second
          // it throws a stupid amount of spurious errors
          highest_id = incoming_messages.map((m) => m.id).reduce(max);
        } catch (e) {
          highest_id = -1;
        }


        if (getCurrentMessageID(roomId) >= highest_id) {
            //If we've already fetched all these messages
            //then don't do it again
            return [];
        }
        setCurrentMessageID(highest_id,roomId);

        // And finally add all of the new messages into the state
        addAll(incoming_messages,roomId);

        // The caller needs to update state
        return incoming_messages;
      } else {
        // If that call was not successful, throw an error.
        throw Exception('Room does not exist');
      }
  }

  Future<List<Message>> fetchMessages() async {
    return fetchRoomMessages(_roomId);
  }

}
