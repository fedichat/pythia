// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:background_fetch/background_fetch.dart';
import 'package:cross_local_storage/cross_local_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart' hide Message;
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'cache.dart';
import 'message.dart';
import 'watch_drawer.dart';


// TODO: this definitely has edge cases
// but how bad can they be right
String escapeJson(String s) {
  return

  s.replaceAll(new RegExp(r'\\'),r'\\')
   .replaceAll(new RegExp(r'\t'),r'\t')
   .replaceAll(new RegExp(r'"'),r'\"')
   .replaceAll(new RegExp(r'\n'),r'\n')
   .replaceAll(new RegExp(r'\r'),r'\r')
   .replaceAll(new RegExp(r'\f'),r'\f')
   //.replaceAll(new RegExp(r'\b'),r'\b');
   ;
}

void main() {
    WidgetsFlutterBinding.ensureInitialized();
    runApp(Pythia());
}

class Pythia extends StatelessWidget {
  String roomId = "1,1";
  String server = "fedichat.firechicken.net";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pythia',
      home: Room()
    );
  }
}

// The state itself
class RoomState extends State<Room> with WidgetsBindingObserver {

  Cache cache = Cache();

  Watcher watch;

  List<Message> _messages = [];

  TextEditingController _controller;
  FlutterLocalNotificationsPlugin notifier;

  LocalStorageInterface _storage;

  Timer _pollTimer;
  final _pollMilliSeconds = 100;
  Timer _slowPollTimer;
  final _slowPollSeconds = 5;

  // TODO: move these into localstorage
  String _homeserver = "fedichat.firechicken.net";
  
  String _roomName = "";
  String _roomTopic = "";

  // Various flags
  bool triggeredMessage = false;
  bool init = false;
  bool active = true;

  // This exists so I can redraw
  // the input box on android
  // Because it doesn't always happen?
  TextStyle ttt = TextStyle(fontSize: 14);

  // TODO Move all defaults here
  // instead of in code
  final _def_username = "anon";
  final _def_topic = "";
  final _def_description = "This room seems unassuming.";
  final _def_roomID = "1,1";

  FocusNode messageNode;

  final helpMessage = '''

/help           Show this message
/nick NAME      Change your name
/connect HOST   Connect to a different host server
/go DIRECTION   Move around fedispace
/go ID          Join a room by ID
/create         Create a room where you are
/look           View the room description at the current location
/describe TEXT  Set the room's description
/topic TOPIC    Change the room's topic
/roomname NAME  Change the room's name
/watch ID NAME  Add a room into your watchlist
/unwatch NAME   Remove a room from your watchlist
''';

  void initState() {
    super.initState();

    // These have to be called before we fetch messages
    // TODO: How does the cache handle local vs full paths?
    cache.setServer(_homeserver);

    // async bad
    // init all things that need to be async
    dumbInitState();

    watch = Watcher(switchRoom);
    messageNode = FocusNode();
    _pollTimer = Timer.periodic(Duration(milliseconds: _pollMilliSeconds), (Timer t) => update());
    _slowPollTimer = Timer.periodic(Duration(seconds: _slowPollSeconds), (Timer t) => backgroundUpdate());
    _controller = TextEditingController();
    WidgetsBinding.instance.addObserver(this);

    // Start listening for new messages
    initBackgroundTask();
  }

  Future<void> initBackgroundTask() async {
    // Configure BackgroundFetch.
    BackgroundFetch.configure(BackgroundFetchConfig(
        // Will this actually run every minute?
        minimumFetchInterval: 1,
        stopOnTerminate: false,
        enableHeadless: true,
        requiresBatteryNotLow: false,
        requiresCharging: false,
        requiresStorageNotLow: false,
        requiresDeviceIdle: false,
        requiredNetworkType: NetworkType.NONE,
        forceAlarmManager: true
    ), (String taskId) async {
      // This is the fetch-event callback.
      backgroundUpdate();
      // IMPORTANT:  You must signal completion of your task or the OS can punish your app
      // for taking too long in the background.
      BackgroundFetch.finish(taskId);
    });

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  // It's dumb because its async
  void dumbInitState() async {
    _storage = await LocalStorage.getInstance();

    cache.setRoom(getRoomID());

    _messages.addAll(await cache.fetchMessages());

    notifier = FlutterLocalNotificationsPlugin();
    // Initialize notifications
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('notification_icon');
    // final IOSInitializationSettings initializationSettingsIOS =
    //     IOSInitializationSettings();
    // final MacOSInitializationSettings initializationSettingsMacOS =
    //     MacOSInitializationSettings();
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        //iOS: initializationSettingsIOS,
        //macOS: initializationSettingsMacOS
    );
    await notifier.initialize(initializationSettings,
        onSelectNotification: selectNotification);
  }
  Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
    switchRoom(payload);
  }


  void dispose() {
    _controller.dispose();
    messageNode.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  String shortRoomId() {
    String _roomId = getRoomID();

    if (_roomId.contains('@')) {
      return "${_roomId.split('@')[0]}";
    } else {
      return _roomId;
    }
  }

  String expandedRoomId() {
    String _roomId = getRoomID();

    if (_roomId.contains('@')) {
      return _roomId;
    } else {
      return "${_roomId.split('@')[0]}@$_homeserver";
    }
  }

  void log(String m) {
    // Don't log empty messages
    if (m == "") {
      return;
    }

    setState(() {
      _messages.add(Message("*",m));
    });
  }

  // Add numbers to the room id
  void add(int x, int y) {
    String _roomId = getRoomID();

    // Save the room ID tail
    var tail;
    // TODO: this could be faster
    if (_roomId == shortRoomId()) {
      tail = "";
    } else {
      tail = "@${_roomId.split('@')[1]}";
    }

    // Get the coordinates as individual strings
    var coords = shortRoomId().split(",");

    // Assume you're not in 1D space
    coords[0] = "${int.parse(coords[0]) + x}";
    coords[1] = "${int.parse(coords[1]) + y}";

    setRoomID("${coords.join(',')}$tail");

  }

  Future<String> getVar(String key) async {
    // This creates a stupid amount of logspam on bad requests
    // no way to turn it off as far as I can tell fml
    var response = await http.get('https://$_homeserver/_fedichat/v1/json/room/${getRoomID()}/state/value/$key');

    if (response.statusCode == 200) {
      return response.body;            
    } else {
      // Might be worth returning a real error here
      return null;
    }
  }

  void setVar(String key, String value) async {
    var response = await http.put('https://$_homeserver/_fedichat/v1/json/room/${getRoomID()}/state/value/$key',body: value);
    if (response.statusCode != 200) {
      // XXX: is this worth logging to the user?
      log("Could not set state variable $key");
      log(response.body);
      return;
    }
  }

  void describeRoom() async {
    final description = await getVar("description") ?? _def_description;
    log(description);
  }

  void createRoom(String id) async {
    var response = await http.post('https://$_homeserver/_fedichat/v1/json/room/$id');
    if (response.statusCode != 200) {
      log("Could not create room.");
      log(response.body);
      return;
    }
  }

  void update() async {
    //Update the topic and room name
    var topic = await getVar("topic") ?? _def_topic;
    if (topic != _roomTopic) {
      setState(() {
        _roomTopic = topic;
      });
    }

    var room = await getVar("roomname") ?? shortRoomId();
    if (room != _roomName) {
      setState(() {
        _roomName = room;
      });
    }

    //Update messages
    try {
      var newMessages = await cache.fetchMessages();
      if (newMessages.length != 0) {
        setState(() {
          _messages.addAll(newMessages);
        });
      }
    } catch(e) {
      if (!triggeredMessage && !init) {
        log("The endless wastes stretch away beneath your eyes. Maybe you should make a room?");
        triggeredMessage = true;
        // Don't init if the room doesn't exist
        init = true;
      }
    }

    if (!init) {
      describeRoom();
      init = true;
    }
  }

  void backgroundUpdate() async {
    // Fetch the messages for all watched rooms
    for (var roomId in watch.getIds()) {

      // Update the cache for each room
      var messages = await cache.fetchRoomMessages(roomId);

      if (messages.length > 0 && roomId != getRoomID()) {
        watch.new_messages(roomId);
      }

      // Check to see if there were any pings
      // Jesus christ why does dart not have string trim stuff
      final username = await getUsername();
      for (var message in messages.map((m) => m.body)) {
        if (message.contains("$username")) {

          // Mark it in the watchlist
          // If it's not the room we're in
          if (roomId != getRoomID()) {
            watch.mark(roomId);
          }

          // And display a notification to the user
          // if we're not the active app and we're not in the active channel
          // TODO: roomID normalization
          //       RoomIds should probably be in a class that does this
          if (!active || (roomId != getRoomID())) {
            const AndroidNotificationDetails androidPlatformChannelSpecifics =
                AndroidNotificationDetails(
                    '0', 'Pings', 'When people say your name in fedichat.',
                    importance: Importance.max,
                    priority: Priority.high,
                    showWhen: true);
            const NotificationDetails platformChannelSpecifics =
                NotificationDetails(android: androidPlatformChannelSpecifics);
            await notifier.show(
                0, watch.getName(roomId), message, platformChannelSpecifics,
                payload: roomId);
          }

        }
      }
    }

  }

  void reset() {
    triggeredMessage = false;
    init = false;
  }

  void sendMessage(String text) async {
      // Initialize shared storage
      // Why can't this be global?

      if (text == "") {
        return;
      }
      var words = text.split(' ');
      switch(words[0]) {

        case "/nick":
          // What happens if the string is just /nick without any arguments
          final nick = words[1];
          _storage.setString('username', nick);

          log("Your nick has been changed to $nick");

          return;

        case "/help":
          log(helpMessage);
          return;

        case "/topic":
          final topic = words.sublist(1).join(" ");
          setVar("topic",topic);
          log("The topic has been changed to $topic");
          return;

        case "/roomname":
          final roomname = words.sublist(1).join(" ");
          setVar("roomname",roomname);
          log("The room name has been changed to $roomname");
          return;

        case "/describe":
          final description = words.sublist(1).join(" ");
          setVar("description",description);
          log("The room description has been changed to $description");
          return;

        case "/look":
          describeRoom();
          return;

        case "/create":
          createRoom(getRoomID());
          log("You have created a new room at ${shortRoomId()}");
          return;

        case "/watch":
          var id = words[1];
          var name = words.sublist(2).join(" ");
          setState(() {
            watch.addRoom(id,name);
          });
          log("You are now watching $id");
          return;

        case "/unwatch":
          var name = words.sublist(1).join(" ");
          setState(() {
            watch.removeRoom(name);
          });
          log("You are no longer watching $name");
          return;

        case "/go":
          final destination = words[1];
          switch(destination) {
            case "north":
            case "up":
              add(0,1);
              break;
            case "south":
            case "down":
              add(0,-1);
              break;
            case "left":
            case "west":
              add(-1,0);
              break;
            case "right":
            case "east":
              add(1,0);
              break;
            default:
              // Check if the room ID is parseable
              // If not check if it's in the watchlist
              // If not check it it's in the directory
              setRoomID(destination);
          }
          switchRoom(getRoomID());
          return;
        case "/connect":
          _homeserver = words[1];
          return;
      }
      final username = await getUsername();

      final response = await http.post("https://$_homeserver/_fedichat/v1/json/room/${getRoomID}/messages", body: '{"type": "text", "body": "${escapeJson(text)}", "from": "${escapeJson(username)}"}');
      if (response.statusCode != 200) {
          throw Exception('Could not send message');
      }

  }

  String getUsername() {
    return _storage.getString('username') ?? _def_username;
  }

  // This gets called before init because it's called by build()
  // I don't know why build is called before init
  // but we just return an empty string then
  String getRoomID() {
    try {
      String r = _storage.getString('roomID') ?? _def_roomID;
      return r;
    } on NoSuchMethodError catch(e) {
      return "";
    }
  }
  void setRoomID(String id) {
    _storage.setString('roomID',id);
  }

  void switchRoom(String id) {
      // Switch rooms
      setRoomID(id);

      // Reset the message cache because it's for a different room
      cache.setRoom(id);
      _messages.clear();
      setState(() {
        _messages.addAll(cache.getMessages());

        // Clear the new message flags for the room
        watch.clearFlags(id);
      });

      reset();

  }

  Widget roomBody(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                  Text(_roomName),
                  Text(_roomTopic, style: TextStyle(fontSize: 12.0)),
              ])
        ),
        body: Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                    Expanded(child: _buildMessages()),
                    TextField(
                        autofocus: true,
                        focusNode: messageNode,
                        textInputAction: TextInputAction.send,
                        style: ttt, 
                        controller: _controller,
                        onSubmitted: (text) {
                            // Do I need this twice?
                            sendMessage(text);
                            // Why does this controller not clear?
                            _controller.clear();
                            FocusScope.of(context).requestFocus(messageNode);
                        },
                        decoration: InputDecoration(
                            hintText: "Enter a message",
                            suffixIcon: IconButton(
                                icon: Icon(Icons.send),
                                onPressed: () {
                                  sendMessage(_controller.text);
                                  _controller.clear();
                                  FocusScope.of(context).requestFocus(messageNode);
                                },
                            ),
                        ),
            // lol indentation
                    )
                ])
          )
    );


  }
  // pause timers in background
  // inactive = pause normal updates
  // same with paused
  // resumed = resume updates
  // detached = probably do nothing lol
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      // Resume the main update loop when we're in the foreground
      _pollTimer = Timer.periodic(Duration(milliseconds: _pollMilliSeconds), (Timer t) => update());
      active = true;
    } else if (state == AppLifecycleState.inactive || state == AppLifecycleState.paused) {
      // Stop the main update loop when we're in the background
      if (_pollTimer != null) {
        _pollTimer.cancel();
      }
      active = false;
      _pollTimer = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return 
        Scaffold(
            appBar: AppBar(
                title: Text('${expandedRoomId()}'),
                actions: [
                  Builder(
                      builder: (context) => IconButton(
                          icon: Icon(Icons.map),
                          onPressed: () => Scaffold.of(context).openEndDrawer(),
                          tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                      ),
                  ),
                ],
            ),
            body: Center(
                child: roomBody(context),
            ),
            endDrawer: Drawer(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: watch.makeWidgets()
                ),
            ),
        );
  }
  Widget _buildMessages() {
    return ListView(
      padding: const EdgeInsets.only(left: 0.0, top: 12.0, bottom: 12),
      reverse: true,
      // TODO: I don't think that this need toList twice
      children: _messages.map((m) => m.toRow()).toList().reversed.toList()
      );
  }
}

// And the widget that uses it
// I think
// lol
class Room extends StatefulWidget {
  @override
  RoomState createState() => RoomState();
}
