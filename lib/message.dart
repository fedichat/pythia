import 'package:flutter/material.dart';

import 'dart:convert';
import 'dart:ui';

// struct
// basically a tuple
// wish it was rust
class Message {
  String from;
  String body;
  String type;
  Map<String, dynamic> props;
  int id = 0;
  // The body should be red if there's a ping
  // and $from should be color coded
  Widget toText() => SelectableText("<$from> $body", style: TextStyle(fontSize: 12, fontFamily: "monospace"));
  Widget toImage() {
    // all matrix images have a url
    // but custom properties aren't saved rip
    var thumb = props["url"].toString();
    if (thumb.startsWith("https://") || thumb.startsWith("http://")) {
      return Image.network(thumb, fit: BoxFit.scaleDown);
    } else if (thumb.startsWith("mxc://")) {
      var s = thumb.replaceFirst(new RegExp(r'mxc://'), '').split("/");
      return Image.network("https://" + s[0] + "/_matrix/media/r0/download/" + s[0] + "/" + s[1],
          fit: BoxFit.scaleDown);
    }

  }
  Widget render() {
    if (this.type == "text") {
      return toText();
    } else if (this.type == "image") {
      return Padding(
          padding: EdgeInsets.only(bottom: 4.0, right: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.only(bottom: 4.0),
                  child: SelectableText("<$from> ", style: TextStyle(fontSize: 12, fontFamily: "monospace")),
              ),
              ConstrainedBox(constraints: BoxConstraints(maxHeight: window.physicalSize.height/2),child: toImage()),
            ])
      );
    } else {
      return toText();
    }
  }
  Widget toRow() => 
          Row(children: [
              Flexible(
                  child: render())
          ]);
  Message(String from, String body) {
    this.from = from;
    this.body = body;
  }
  Message.fromJson(Map<String, dynamic> json) {
    // These are UTF-8 but dart likes Latin-1
    // Who even fucking uses Latin-1
    // The strings get manually converted here to a sane encoding
    this.from = utf8.decode(json["from"].toString().codeUnits);
    this.body = utf8.decode(json["body"].toString().codeUnits);
    this.type = utf8.decode(json["type"].toString().codeUnits);
    this.id = json["message_id"];
    this.props = json;
  }
}
